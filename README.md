# Cons2.TERMITES39X hex input - shuffled and processed

## Shuffle process used

The script multiput0x.py exists in the following repositories and uses a onetime key of `2112312020190909`

+[sal1520onetime44](https://bitbucket.org/wrightsolutions/sal1520onetime44)

+[sal683onetime31](https://bitbucket.org/wrightsolutions/sal683onetime31)  ... older and less preferred version




## Input hashes plus Rowcounts and why not a round 100000 or 200000 in every case?

Each shuffled version of the hex of TERMITES39X is pre-validated to ensure entropy (usually passes) and
decimal equivalent is sufficiently sized ( >= DECMIN, and <= DECMAX )

Failures have been removed, reducing the round 100000 input to a bit less.

Directory rows100000/ contains the shuffled hex we will use as input to the hashing process


## Source as csv

Because the csv source takes up a bit more space than the results output I have not provided it in every case so
as not to bloat the repo too much

Directory rowsource/ has the csv data for some of the hashing runs.


## Output samples

Directory ver32multiput0x/ contains the results of the hashing process


## Output uniqueness - smallish sample to demonstrate lack of clashes

```
sort sal1520termites39shuffled34546wanhex.txt | uniq | wc -l
34546
```

Above example is for regular power [ver_minor=0]

We do not aim to guarantee lack of clashing between different power levels,  
however as long as all your batch run is executed with the same [plug] power level  
given by ver_minor, then you should observe no clashes

In a high security setting, you should be running double trebled or triple trebled only, and both of those will
have a level of clashes that is theoretical, but tiny in comparison to many millions of rows of input.


## Hints about rejection rates - very approximate

+ Seeing 1% rejection in a 10000 row input fits with what we might expect with the new flexible entropy implementation
+ Seeing 10% rejection suggests that the hex you are supplying has not been vetted to ensure it is stringable (on input) to base64

Our entropy mechanism, and I suppose the hashing itself, relies on a consistent base64 representation of the input ( decimal / hex )

You should pre-filter hex that is not paired or countable to a standard that supports translation to base64, or,   
use the harness_prevalidator.py script to test your row before including it in a file of hex for multiput0x.py


